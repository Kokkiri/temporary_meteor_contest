import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { withStyles } from '@material-ui/core/styles';
import { Link, Route, BrowserRouter, Switch } from 'react-router-dom';
import { Select, CandidatListComponent, Connexion, SignIn } from '../pages';
import { List } from '../../api/list.js';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Meteor } from 'meteor/meteor';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuAca: null,
            academie: '',
        };
    }

    render() {  
        moment.locale('fr');
        const { classes } = this.props;

        const GenConnexion = ({ match }) => {
            return (
                <Switch>
                    <Route exact path={match.url}>
                        <Connexion
                            candidat={this.get_candidat_by_name(match.params.candidat)}
                        />
                    </Route>
                </Switch>
            );
        };

        const CandidateByList = ({ match }) => {
            return (
                <Switch>
                    <Route exact path={match.url}>
                        {match.path === '/commission/:numCom' ? (
                            <CandidatListComponent
                                candidatList={this.get_candidatList(match.params.numCom)}
                                path="/commission"
                                tirdColumnTitle="centre d'accueil du candidat"
                                place={`pour le Concours ${this.get_concours(
                                    match.params.numCom
                                )} - Commission ${match.params.numCom} `}
                                commission={match.params.numCom}
                            />
                        ) : (
                                <CandidatListComponent
                                    candidatList={this.get_candidatList(match.params.nomCenter)}
                                    path="/centredexamen"
                                    tirdColumnTitle="commission de jury"
                                    place={`du centre d'accueil ${
                                        match.params.nomCenter
                                        } de l'académie ${this.get_academie(match.params.nomCenter)}`}
                                />
                            )}
                    </Route>
                    <Route path={match.url + '/:candidat'} component={GenConnexion} />
                </Switch>
            );
        };

        const Commission = () => {
            return (
                <Switch>
                    <Route exact path="/commission">
                        <Select
                            data={this.get_list_commission()}
                            title="Liste des commissions de jury"
                        />
                    </Route>
                    <Route path="/commission/:numCom" component={CandidateByList} />
                </Switch>
            );
        };

        const CentreDexamen = () => {
            return (
                <Switch>
                    <Route exact path="/centredexamen">
                        <Select
                            data={this.get_list_examen()}
                            title={
                                this.state.academie
                                    ? `Liste des centres d'accueil de l'académie de ${this.state.academie}`
                                    : "Liste de tous les centres d'examens"
                            }
                        />
                    </Route>
                    <Route path="/centredexamen/:nomCenter" component={CandidateByList} />
                </Switch>
            );
        };

        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/home">
                        <div className={classes.root}>
                            <div className={classes.space}></div>
                            {this.props.currentUser ? (
                                <div>
                                    <Button
                                        className={classes.button}
                                        color="primary"
                                        variant="outlined"
                                        component={Link}
                                        to="/commission"
                                    >
                                        Jury
                                    </Button>
                                    <div className={classes.space}></div>
                                    <Button
                                        className={classes.button}
                                        aria-controls="simple-menu"
                                        aria-haspopup="true"
                                        color="primary"
                                        variant="outlined"
                                        onClick={this.handleMenuAcaClick}
                                    >
                                        Candidat...
                                    </Button>
                                    <Menu
                                        id="simple-menu"
                                        anchorEl={this.state.menuAca}
                                        keepMounted
                                        open={Boolean(this.state.menuAca)}
                                        onClose={this.handleMenuAcaClose}
                                    >
                                        {this.get_list_academie().map(a => (
                                            <MenuItem
                                                key={a}
                                                component={Link}
                                                to="/centredexamen"
                                                onClick={this.handleAcaClick}
                                            >
                                                {a}
                                            </MenuItem>
                                        ))}
                                    </Menu>
                                    <div className={classes.space}></div>
                                    <Button
                                        className={classes.disconnect}
                                        aria-controls="simple-menu"
                                        aria-haspopup="true"
                                        color="secondary"
                                        variant="outlined"
                                        onClick={Meteor.logout}
                                        component={Link}
                                        to={"/signin"}
                                    >
                                        Déconnexion
                                    </Button>
                                </div>
                            ) : (
                                    ''
                                )}
                        </div>
                    </Route>
                    <Route path="/commission" component={Commission} />
                    <Route path="/centredexamen" component={CentreDexamen} />
                    <Route path="/signin" component={SignIn} />
                </Switch>
            </BrowserRouter>
        );
    }

    handleAcaClick = event => {
        this.setState({ academie: event.currentTarget.textContent, menuAca: null });
    };

    handleMenuAcaClick = event => {
        this.setState({ menuAca: event.currentTarget });
    };

    handleMenuAcaClose = () => {
        this.setState({ menuAca: null });
    };

    get_list_academie = () => {
        const data = this.props.list;
        const tab = [];
        data.map(el => {
            if (tab.indexOf(el.academie) === -1) {
                tab.push(el.academie);
            }
        });
        return tab.sort();
    };

    get_list_commission = () => {
        const data = this.props.list;
        const tab = [];
        data.map(el => {
            if (tab.find(x => x.link == el.num_com) === undefined) {
                tab.push({
                    view: `Concours ${el.concours} - Commission ${el.num_com}`,
                    link: el.num_com,
                });
            }
        });
        return tab.sort((a, b) => {
            return a.link > b.link;
        });
    };

    academie_filter = list_to_filter => {
        if (this.state.academie === '') {
            return list_to_filter;
        } else {
            return list_to_filter.filter(el => el.academie === this.state.academie);
        }
    };

    get_list_examen = () => {
        const data = this.props.list;
        const tab = [];
        this.academie_filter(data).map(el => {
            if (tab.find(x => x.link == el.c_examen) === undefined) {
                tab.push({
                    view: el.c_examen,
                    link: el.c_examen,
                });
            }
        });
        return tab.sort((a, b) => {
            return a.link > b.link;
        });
    };

    get_candidatList = value => {
        const data = this.props.list;
        let tab = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].color === undefined) {
                Meteor.call('list.update', data[i]._id, false);
            }
            if (data[i].num_com !== 'undefined' && value === data[i].num_com) {
                tab.push(data[i]);
            }
            if (data[i].concours !== 'undefined' && value === data[i].concours) {
                tab.push(data[i]);
            }
            if (data[i].c_examen !== 'undefined' && value === data[i].c_examen) {
                tab.push(data[i]);
            }
        }
        return tab;
    };

    get_candidat_by_name = name => {
        const data = this.props.list;
        let cand = {};
        data.map((el, i) => {
            if (name === el.candidat) {
                cand = el;
            }
        });
        return cand;
    };

    get_concours = num_commission => {
        const data = this.props.list;
        const result = data.find(({ num_com }) => num_com === num_commission);
        if (result === undefined) {
            return '';
        } else {
            return result.concours;
        }
    };

    get_academie = nom_centre => {
        const data = this.props.list;
        const result = data.find(({ c_examen }) => c_examen === nom_centre);
        if (result === undefined) {
            return '';
        } else {
            return result.academie;
        }
    };

}

const styles = theme => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '400px',
    },
    space: {
        margin: theme.spacing(3),
    },
    button: {
        width: '300px',
        height: '100px',
        fontSize: '20px',
    },
    disconnect: {
        width: '150px',
        height: '50px',
        fontSize: '15px',
    }
});

const track = withTracker(() => {
    Meteor.subscribe('list');

    return {
        list: List.find({}).fetch(),
        currentUser: Meteor.user(),
    };
})(Home);

const HomeWithStyles = withStyles(styles)(track);
export { HomeWithStyles as Home };
