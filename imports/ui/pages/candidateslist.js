import React, { Component } from 'react';
import { Session } from 'meteor/session';
import PropTypes from 'prop-types';
import { withRouter, Redirect, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

class CandidatListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      disable: true,
      codeJuryOk: undefined,
      searchInput: '',
      startDate: '',
      endDate: '',
      uniqDate: [],
      steps: {},
      rangeLength: 10,
    };
    this.handleCodeJury = this.handleCodeJury.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.checkCodeJury = this.checkCodeJury.bind(this);
  }

  render() {
    if (this.props.path === '/commission') {
      if (this.state.codeJuryOk === true) {
        return this.renderList();
      } else if (this.state.codeJuryOk === false) {
        return this.renderAskCodeJury();
      } else {
        // check if jury code has already been given
        this.checkCodeJury();
        return this.renderCheckCode();
      }
    } else {
      return this.renderList();
    }
  }

  checkCodeJury() {
    let codes = Session.get('codes_jury');
    if (codes === undefined) {
      this.setState({ codeJuryOk: false });
      return;
    }
    const codeJury = codes[this.props.commission];
    if (codeJury === undefined) {
      this.setState({ codeJuryOk: false });
      return;
    }
    Meteor.call(
      'list.checkCode',
      this.props.commission,
      codeJury,
      (err, res) => {
        if (err) {
          console.log(err);
        } else {
          this.setState({ codeJuryOk: res });
        }
      }
    );
  }

  handleCodeJury() {
    const codeJury = this.codeJury.value;
    Meteor.call(
      'list.checkCode',
      this.props.commission,
      codeJury,
      (err, res) => {
        if (err) {
          console.log(err);
        } else {
          if (res === true) {
            // code OK : store in session for future checks
            let codes = Session.get('codes_jury');
            if (codes === undefined) codes = {};
            codes[this.props.commission] = codeJury;
            Session.set('codes_jury', codes);
            this.setState({ codeJuryOk: true });
          } else {
            // si non valide : afficher un message
            console.log('MAUVAIS CODE');
            this.codeJury.value = '';
          }
        }
      }
    );
  }

  handleKeyUp(evt) {
    if (evt.keyCode === 13) this.handleCodeJury();
  }

  renderAskCodeJury() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        {this.renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>
            Saisie du code pour la commission n°{this.props.commission}
          </p>
        </div>
        <div className={classes.functions}>
          <TextField
            autoFocus
            className={classes.button}
            placeholder="Code du jury"
            inputRef={c => {
              this.codeJury = c;
            }}
            onKeyUp={this.handleKeyUp}
          />
          <Button
            className={classes.button}
            onClick={this.handleCodeJury}
            color="secondary"
            variant="outlined"
          >
            Valider
          </Button>
        </div>
        <div className={classes.toLeft}>
          <Button
            className={classes.button}
            onClick={this.setRedirect}
            color="secondary"
            variant="outlined"
          >
            Retour
          </Button>
        </div>
      </Paper>
    );
  }

  renderCheckCode() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        {this.renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>Vérification du code en cours...</p>
        </div>
      </Paper>
    );
  }

  renderList() {
    const { classes, place } = this.props;
    const { searchInput, startDate, endDate, rangeLength, steps } = this.state;
    const len = rangeLength - 1;
    const eachCandidat = () => {
      const list = this.candidatsFilter(searchInput, startDate, endDate).map(
        el => {
          let tirdColumn = '';
          if (this.props.path === '/commission') {
            tirdColumn = el.c_examen;
          } else {
            tirdColumn = `(${el.num_com}) ${el.concours}`;
          }
          let color = '';
          if (el.color === true) {
            color = classes.secondary;
          } else {
            color = classes.primary;
          }
          return (
            <TableRow key={el.candidat} hover={true}>
              <TableCell>{this.linkToVideoConf(el, color)}</TableCell>
              <TableCell>{moment(el.date).format('ll')}</TableCell>
              <TableCell>{el.heure}</TableCell>
              <TableCell>{el.candidat}</TableCell>
              <TableCell>{tirdColumn}</TableCell>
            </TableRow>
          );
        }
      );
      return list;
    };
    return (
      <Paper className={classes.root}>
        {this.renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>Liste des candidats {place}</p>
          <div className={classes.titleRight}>
            <TextField
              autoFocus
              className={classes.button}
              placeholder="Recherche"
              onChange={this.handleSearchBar}
              value={searchInput}
            />
            <Button
              className={classes.button}
              onClick={this.setRedirect}
              color="secondary"
              variant="outlined"
            >
              Retour
            </Button>
          </div>
        </div>

        <div className={classes.functions}>
          <div className={classes.toCenter}>
            <p style={{ color: '#989898' }}>Sélection des dates</p>
            <Range
              className={classes.unselectable}
              marks={steps}
              dots={true}
              allowCross={false}
              min={0}
              onAfterChange={this.getDateRange}
              max={len}
              defaultValue={[0, len]}
              tipFormatter={value => this.dateTipFormatter(value)}
              tipProps={{
                placement: 'top',
                prefixCls: 'rc-slider-tooltip',
              }}
            />
          </div>
        </div>

        <div className={classes.table}>
          <Table className={classes.unselectable}>
            <TableHead>
              <TableRow className={classes.cell}>
                <TableCell></TableCell>
                <TableCell>date de convocation</TableCell>
                <TableCell>horaire de convocation</TableCell>
                <TableCell>candidats</TableCell>
                <TableCell>{this.props.tirdColumnTitle}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{eachCandidat()}</TableBody>
          </Table>
        </div>

        <div className={classes.toLeft}>
          <Button
            className={classes.button}
            onClick={this.setRedirect}
            color="secondary"
            variant="outlined"
          >
            Retour
          </Button>
        </div>
      </Paper>
    );
  }

  setRedirect = () => {
    this.setState({ redirect: true });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to={this.props.path} />;
    }
  };

  handleSearchBar = e => {
    let userInput = e.target.value;
    this.setState({ searchInput: userInput });
  };

  // sort dates without duplicates
  componentWillMount() {
    const { candidatList } = this.props;
    //this.props.groupByValue()
    candidatList.sort((a, b) => {
      return a.date > b.date ? 1 : a.date < b.date ? -1 : 0;
    });
    const date = [];
    const steps = {};
    candidatList.map(el => date.push(el.date));
    // deletion of duplicates
    let uniq = a => [...new Set(a)];
    // define labels by steps
    uniq(date).map((el, i) => {
      steps[i] = <p style={{ width: '80px' }}>{moment(el).format('D MMM')}</p>;
    });
    const rangeLength = uniq(date).length;
    this.setState({
      uniqDate: uniq(date),
      steps: steps,
      rangeLength: rangeLength,
    });
  }

  // get label for tipFormat from rangeSlide
  dateTipFormatter = value => {
    return moment(this.state.uniqDate[value]).format('D MMM YY');
  };

  // Get value of tipFormat to list candidates by selected range date
  getDateRange = value => {
    let start = this.state.uniqDate[value[0]];
    let end = this.state.uniqDate[value[1]];

    this.setState({ startDate: start, endDate: end });
  };

  // (CandidatListComponent) filter per hour of passage + depending on whether they have already passed or not
  candidatsFilter = (searchInput, startDate, endDate) => {
    const { candidatList } = this.props;
    candidatList.sort((a, b) => {
      if (a.color === false && b.color === true) {
        return -1; //false
      }
      if (a.color === true && b.color === false) {
        return 1; //true
      }
      if (a.color === b.color) {
        if (a.date === b.date) {
          return a.heure > b.heure ? 1 : a.heure < b.heure ? -1 : 0;
        }
        return a.date > b.date ? 1 : a.date < b.date ? -1 : 0;
      }
    });
    let ele = candidatList.filter(el => {
      let date = moment(el.date).format('D MMM YY');
      let list = el.candidat
        .concat(el.heure)
        .concat(el.c_examen)
        .concat(el.num_com)
        .concat(el.concours)
        .concat(date)
        .toLowerCase();
      if (startDate === '' && endDate === '') {
        return list.indexOf(searchInput.toLowerCase()) !== -1;
      }
      if (el.date >= startDate && el.date <= endDate) {
        return list.indexOf(searchInput.toLowerCase()) !== -1;
      }
    });
    return ele;
  };

  linkToVideoConf = (el, color) => {
    return (
      <Button
        className={color}
        component={Link}
        to={`${this.props.match.url}/${el.candidat}`}
        variant="outlined"
      >
        Connexion
      </Button>
    );
  };
}

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  functions: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: '30px',
  },
  unselectable: {
    userSelect: 'none',
    mozUserSelect: 'none',
    webkitUserSelect: 'none',
    msUserSelect: 'none',
  },
  cell: {
    textTransform: 'uppercase',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  toLeft: {
    width: '15%',
    margin: `${theme.spacing.unit}px 0`,
  },
  toRight: {
    width: '10%',
    margin: `${theme.spacing.unit}px 0`,
  },
  toCenter: {
    width: '100%',
    margin: `${theme.spacing.unit}px 50px`,
  },
  button: {
    margin: theme.spacing(3),
  },
  table: {
    margin: `0px ${theme.spacing(3)}px`,
  },
  primary: {
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
  },
  secondary: {
    color: theme.palette.secondary.purple,
    borderColor: theme.palette.secondary.purple,
  },
});

CandidatListComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledCandidatListComponent = withStyles(styles)(CandidatListComponent);
const routedCandidatListComponent = withRouter(styledCandidatListComponent);
export { routedCandidatListComponent as CandidatListComponent };
