import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import history from '../layouts/history'

validate.options = {
    fullMessages: false,
};

const schema = {
    user: {
        presence: { allowEmpty: false, message: 'validatejs.isRequired' },
        length: {
            maximum: 64,
        },
    },
    password: {
        presence: { allowEmpty: false, message: 'validatejs.isRequired' },
        length: {
            maximum: 64,
        },
    },
}


function SignIn({loggingIn}) {
    const classes = useStyles();
    
    const [formState, setFormState] = useState({
        isValid: false,
        values: {},
        errors: {},
    });
    
    useEffect(() => {
        const errors = validate(formState.values, schema);
        
        setFormState(() => ({
            ...formState,
            isValid: !errors,
            errors: errors || {},
        }));
    }, [formState.values]);
    
    const [openError, setOpenError] = useState(false);
    
    const handleChange = (event) => {
        event.persist();
        
        setFormState(() => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value,
            }
        }));
    };
    
    const handleSignIn = (event) => {
        event.preventDefault();
        if (formState.isValid === true) {
            const { user, password } = formState.values;
            Meteor.loginWithPassword(user, password, (err) => {
                console.log('user', Meteor.userId())
                if (err) {
                    setOpenError(true);
                } else {
                    history.push("/home")
                }
                
            });
        }
    };
    
    const handleErrorClose = (reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
    };
    
    const hasError = (field) => !!(formState.errors[field]);
    return (
        <>
            <form onSubmit={handleSignIn} className={classes.form}>
                <TextField
                    autoFocus
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="user"
                    label="User"
                    type="text"
                    id="user"
                    autoComplete="current-user"
                    error={hasError('user')}
                    helperText={hasError('user') ? "Donner un nom d'utilisateur" : null}
                    onChange={handleChange}
                    value={formState.values.user || ''}
                    disabled={loggingIn}
                    />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    error={hasError('password')}
                    helperText={hasError('password') ? "format incorrect" : null}
                    onChange={handleChange}
                    value={formState.values.password || ''}
                    disabled={loggingIn}
                    />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={!formState.isValid || loggingIn}
                    >
                Connexion</Button>
            </form>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={openError}
                autoHideDuration={4000}
                onClose={handleErrorClose}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    className: classes.error,
                }}
                message={"error"}
                />
        </>
    );
}

const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        position: 'relative',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
}));

export default withTracker(() => {
    const loggingIn = Meteor.loggingIn();
    return {
        loggingIn,
    };
})(SignIn);

SignIn.defaultProps = {
    loggingIn: false,
};

SignIn.propTypes = {
    loggingIn: PropTypes.bool,
};