import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter, Redirect, Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

class Select extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      searchInput: '',
    };
  }

  render() {
    const { classes } = this.props;
    const { searchInput } = this.state;

    const listNoms = () => {
      const list = this.listFilter(searchInput).map(el => {
        return (
          <TableRow key={el.link} hover={true}>
            <TableCell>{this.linkToCandidat(el.link)}</TableCell>
            <TableCell>{el.view}</TableCell>
          </TableRow>
        );
      });
      return list;
    };

    return (
      <Paper className={classes.root}>
        {this.renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>{this.props.title}</p>
          <div className={classes.titleRight}>
            <TextField
              autoFocus
              className={classes.button}
              placeholder="Recherche"
              onChange={this.handleSearchBar}
              value={searchInput}
            />
            <Button
              className={classes.button}
              onClick={this.setRedirect}
              color="secondary"
              variant="outlined"
            >
              Retour
            </Button>
          </div>
        </div>
        <div>
          <Table>
            <TableBody>{listNoms()}</TableBody>
          </Table>
        </div>
        <div className={classes.toLeft}>
          <Button
            className={classes.button}
            onClick={this.setRedirect}
            color="secondary"
            variant="outlined"
          >
            Retour
          </Button>
        </div>
      </Paper>
    );
  }

  setRedirect = () => {
    this.setState({ redirect: true });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/home" />;
    }
  };

  handleSearchBar = e => {
    let userInput = e.target.value;
    this.setState({ searchInput: userInput });
  };

  listFilter = searchInput => {
    let list = this.props.data;
    let ele = list.filter(el => {
      let name = '';
      if (searchInput !== undefined) {
        name = el.view.toLowerCase();
      }
      return name.indexOf(searchInput.toLowerCase()) !== -1;
    });
    return ele;
  };

  linkToCandidat = value => {
    return (
      <Button
        component={Link}
        to={this.props.match.url + `/${value}`}
        color="primary"
        variant="outlined"
      >
        voir
      </Button>
    );
  };
}

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  color: {
    background: '#99ddff',
  },
  toLeft: {
    width: '15%',
    margin: `${theme.spacing.unit}px 0`,
  },
  button: {
    margin: theme.spacing(3),
  },
  searchBar: {
    width: '10%',
    margin: theme.spacing(3),
  },
  table: {
    margin: `0px ${theme.spacing(3)}px`,
    width: '500px',
  },
});

const styledSelect = withStyles(styles)(Select);
const routedSelect = withRouter(styledSelect);
export { routedSelect as Select };
