import React from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import SignIn from '../pages/signin'
import { makeStyles } from '@material-ui/core/styles';
import history from './history'
import { Route, Router, Switch } from 'react-router-dom';
import { Home } from '../pages/home'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    position: 'relative',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  }
}));


export function App() {
  const classes = useStyles();
  return (
    <div>
          <Router history={history}>
            <Switch>
              <Route exact path="/home" component={Home} />
              <Grid container component="main" className={classes.form}>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                  <Route exact path="/signin" component={SignIn} />
                </Grid>
              </Grid>
            </Switch>
          </Router>
    </div>
  )
}
