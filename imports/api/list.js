import { check } from 'meteor/check';
import data from '../../data.json';

export const List = new Mongo.Collection('list');

List.publicFields = {
  _id: 1,
  academie: 1,
  c_examen: 1,
  candidat: 1,
  date: 1,
  heure: 1,
  concours: 1,
  num_com: 1,
  color: 1,
};

if (Meteor.isServer) {
  Meteor.publish('list', function() {
    if (List.find().count() < 1) {
      data.map(el => {
        List.insert({ ...el });
      });
    }
    yesturday = moment()
      .subtract(1, 'd')
      .format(); // -1 pour prendre en compte le jour courant (bizar avec gte !)
    end = moment()
      .add(1, 'M')
      .format();
    return List.find(
      {
        date: {
          $gte: yesturday,
          $lt: end,
        },
      },
      { fields: List.publicFields }
    );
  });
}

Meteor.methods({
  'list.update'(id, color) {
    check(id, String);
    check(color, Boolean);
    List.update(id, { $set: { color: color } });
  },
  'list.checkCode'(commission, codeJury) {
    check(commission, String);
    check(codeJury, String);
    item = List.findOne({ num_com: commission });
    if (item === undefined) return false;
    return item.code_jury === codeJury;
  },
});
