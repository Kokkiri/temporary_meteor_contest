import { Accounts } from 'meteor/std:accounts-ui';
import { Session } from 'meteor/session';

Accounts.config({
  forbidClientAccountCreation: true,
});

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY',
});

Accounts.onLogout(() => {
  // cleanup session on logout (codes_jury)
  Object.keys(Session.keys).forEach(function(key) {
    Session.set(key, undefined);
  });
  Session.keys = {};
});
